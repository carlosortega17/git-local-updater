Un script en Python que actualiza todos los repos en local, se agrega la ruta de la carpeta que contenga todos los repos de un proyecto.

### Los prerequisitos:

- Python 3
- Git
- Pnpm

### Flags
- `--no-install` | No verifica si los proyectos son pnpm y no realiza la instalacion
- `--stash` | Descarta los cambios en los repositorios para poder cambiar a la rama Dev (esto elimina cualquier cambio actual en el repositorio)

### Comando

- Para actualizar los repositorios que tengan el pnpm, se puede agregar un flag

```bash
python3 main.py {{path}} --{{flag}}
```
