from subprocess import run
from shutil import rmtree
from sys import argv
from os import path, listdir, mkdir
from json import dumps, loads

REPORT_PATH = "repos.json"

def get_repos():
  if not path.exists(REPORT_PATH):
    return []
  with open(REPORT_PATH, "r") as f:
    json_data = loads(f.read())
  return json_data

def check_folder(folder: str):
  pnpm_path = path.join(folder, "pnpm-lock.yaml")
  package_path = path.join(folder, "package.json")
  if not path.exists(package_path) or not path.exists(pnpm_path):
    return False
  return True

def download_repos():
  print("Downloading repos...")
  if not path.exists(argv[1]):
    mkdir(argv[1])
  wms = get_repos()["repositories"]["wms"]
  for type in wms["types"]:
    for repository in type['repos']:
      url = str("{0}/{1}/{2}").format(wms["baseURL"], type["type"], repository)
      run(["git", "clone", url, "--quiet"], cwd=argv[1])
  print("Done downloading repos...")

def execute_pnpm_stage(folder: str):
  print("Executing pnpm install...")
  run(["pnpm", "install"], cwd=folder)
  print("Done executing pnpm install...")

def execute_git_stage(folder: str, stash: bool = False):
  git_path = path.join(folder, ".git")
  if path.exists(git_path):
    print("Executing git update...")
    if stash:
      run(["git", "stash", "--quiet"], cwd=folder)
    run(["git", "switch", "dev", "--quiet"], cwd=folder)
    run(["git", "pull", "origin", "dev", "--quiet"], cwd=folder)
    print("Done executing git update...")
  
def update(enable_check: bool, stash: bool = False):
  dirs = listdir(argv[1])
  for dir in dirs:
    path_dir = path.join(argv[1], dir)
    execute_git_stage(path_dir, stash)
    if enable_check and check_folder(path_dir):
      execute_pnpm_stage(path_dir)
    print("Done updating " + str(path_dir) + "...")

def main():
  enable_check = True
  stash = False
  if len(argv) == 3 and str(argv[2]) == "--no-install":
    enable_check = False
  if len(argv) == 3 and str(argv[2]) == "--stash":
    stash = True
  if len(argv) == 4 and str(argv[2]) == "--no-install" and str(argv[3]) == "--stash":
    enable_check = False
    stash = True
  if len(argv) == 4 and str(argv[2]) == "--stash" and str(argv[3]) == "--no-install":
    enable_check = False
    stash = True
  if len(argv) == 4 and str(argv[2]) == "--no-install":
    enable_check = False
  if len(argv) == 4 and str(argv[2]) == "--stash":
    stash = True
  if len(argv) < 2:
    print("Usage: python updater.py <path to update>")
    return 1
  if not path.exists(argv[1]):
    print("Path not found")
    return 1
  if not path.isdir(argv[1]):
    print("Path is not a directory")
    return 1
  update(enable_check, stash)
  print("Successfully path updated")
  return 0

if __name__ == "__main__":
  main()
  #download_repos()